### Build

* Using your favourite editor, the projec should already have JAR build parameters.
* Otherwise follow standard JAR building procedures (e.g. jar cf jar-file input-file(s))
* Running the project in IntelliJ or Eclipse etc will also suffice.

### Tools
* OS: Ubuntu 18.10
* Editor: IntelliJ Community 2019
* Java version: 11
* Testing: JUnit 5

### Ethos

A basic implementation of the classic Tic-Tac-Toe game with a player and a simple AI opponent. 

The game board (and accompanying logic), AI, and game progression were split into separate classes.

### Testing
Unit tests for all classes were created and public methods were tested for various use cases.

### Improvements
* AI is too simple and needs to use proper tactics.
* Logic in Grid class could be further split in other classes.
* Testing could be more extensive (e.g. individual tests for direct private method testing)
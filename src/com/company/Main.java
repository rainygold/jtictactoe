package com.company;

import com.company.main.AI;
import com.company.main.Game;
import com.company.main.Grid;

public class Main {

    // entry-point for app
    public static void main(String[] args) {

        // setup
        Game game = new Game();
        AI roboPlayer = new AI();
        Grid gameBoard = new Grid();
        gameBoard.populateTheBoard();
        System.out.println(game.startAndRunGame(gameBoard, roboPlayer));
    }
}

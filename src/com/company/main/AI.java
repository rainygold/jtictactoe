package com.company.main;

import java.util.Random;

public class AI {

    // class variables
    private Boolean isTheAITurn;
    private String mark;

    // getters and setters
    Boolean getTheAITurn() {
        return isTheAITurn;
    }

    void setTheAITurn(Boolean theAITurn) {
        isTheAITurn = theAITurn;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public AI() {
        this.isTheAITurn = false;
        this.mark = "O";
    }

    // class methods

    // randomly places a mark on an existing board
    public void randomMark(Grid grid) {

        // gen random numbers to use for determining the mark..
        Random random = new Random();
        int randomRowNo = random.nextInt(2) + 1;
        int randomSpaceNo = random.nextInt(2) + 1;

        // if the space is unoccupied then occupy it - else try a new random combo
        if (grid.checkForOccupancy(randomRowNo, randomSpaceNo)) {
            grid.placeMark(randomRowNo, randomSpaceNo, this.mark);
        } else {
            randomMark(grid);
        }
    }
}

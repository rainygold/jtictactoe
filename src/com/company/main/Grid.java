package com.company.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// represents the game board
public class Grid {

    // class variables
    private HashMap<Integer, ArrayList<String>> board;
    private Boolean playerWon;

    // getters/setters

    public Boolean getPlayerWon() {
        return playerWon;
    }

    public void setPlayerWon(Boolean playerWon) {
        this.playerWon = playerWon;
    }

    public HashMap getBoard() {
        return board;
    }

    public void setBoard(HashMap<Integer, ArrayList<String>> board) {
        this.board = board;
    }

    // constructor
    public Grid() {
        this.board = new HashMap<>();
        this.playerWon = false;
    }

    // class methods

    // gives a Grid a base state to start a game
    public void populateTheBoard() {

        // spaces will be represented by arraylists with numbers as their id
        // integers are row ids
        var row = new ArrayList<String>();
        var row2 = new ArrayList<String>();
        var row3 = new ArrayList<String>();

        // add all empty spaces to the rows
        for (int i = 0; i < 3; i++) {
            row.add("-");
            row2.add("-");
            row3.add("-");
        }

        this.board.put(1, row);
        this.board.put(2, row2);
        this.board.put(3, row3);
    }

    // places a mark on the Grid
    public void placeMark(int rowNo, int spaceNo, String mark) {
        this.board.get(rowNo).set(spaceNo, mark);
    }

    // checks the Grid for a specific row/space combination
    private Boolean checkForMark(int rowNo, int spaceNo, String mark) {
        return this.board.get(rowNo).get(spaceNo).equals(mark);
    }

    // checks the Grid for occupancy
    Boolean checkForOccupancy(int rowNo, int spaceNo) {
        return this.board.get(rowNo).get(spaceNo).equals("-");
    }

    // print the game board
    void printTheBoard() {
        for (ArrayList row : this.board.values()) {
            System.out.println(row);
        }
    }

    // checks Grid state for a winner
    public Boolean checkTheBoard() {

        // check all the rows..
        for (Map.Entry<Integer, ArrayList<String>> entry : this.board.entrySet()) {

            // check each space
            for (String space : entry.getValue()) {

                // check adjacent rows if not default value
                if (!space.equals("-")) {
                    if (checkRows(entry.getValue().indexOf(space), entry.getKey())) {
                        if (space.equals("X")) {
                            this.playerWon = true;
                        }
                        return true;
                    }
                }
            }
        }

        // no winner
        return false;

    }

    // checks adjacent rows according to input
    private Boolean checkRows(int spaceNo, int rowNo) {

        // check spaces according to row

        return checkSpacesHori(spaceNo, rowNo) || checkSpacesVert(spaceNo, rowNo) || checkSpacesDiag(spaceNo, rowNo);
    }

    // check horizontally
    private Boolean checkSpacesHori(int spaceNo, int rowNo) {

        // only requires the values of one row
        for (String spaceValue : this.board.get(rowNo)) {
            if (!spaceValue.equals(this.board.get(rowNo).get(spaceNo))) {
                return false;
            }
        }

        return true;
    }

    // check vertically
    private Boolean checkSpacesVert(int spaceNo, int rowNo) {

        // for code clarity
        var valueToMatch = this.board.get(rowNo).get(spaceNo);

        // only requires the values of one row (but three lists)
        switch (rowNo) {

            case 1:
                if (checkForMark(2, spaceNo, valueToMatch) && checkForMark(3, spaceNo, valueToMatch)) {
                    return true;
                }
                break;
            case 2:
                if (checkForMark(1, spaceNo, valueToMatch) && checkForMark(3, spaceNo, valueToMatch)) {
                    return true;
                }
                break;
            case 3:
                if (checkForMark(2, spaceNo, valueToMatch) && checkForMark(1, spaceNo, valueToMatch)) {
                    System.out.println(rowNo + " " + this.board.get(3).get(spaceNo));
                    return true;
                }
                break;
            default:
                break;
        }
        return false;
    }

    // check diagonally
    private Boolean checkSpacesDiag(int spaceNo, int rowNo) {

        // for code clarity
        var valueToMatch = this.board.get(rowNo).get(spaceNo);

        switch (spaceNo) {

            // rowno with spaceno allows us to determine which direction to check
            case 1:

                if (rowNo == 1) {
                    if (checkForMark(2, 1, valueToMatch) && checkForMark(3, 2, valueToMatch)) {
                        return true;
                    }
                }
                if (rowNo == 3) {
                    if (checkForMark(2, 1, valueToMatch) && checkForMark(1, 2, valueToMatch)) {
                        return true;
                    }
                }
                break;

            case 2:

                if (checkForMark(1, 2, valueToMatch) && checkForMark(3, 0, valueToMatch)) {
                    return true;
                }

                if (checkForMark(1, 0, valueToMatch) && checkForMark(3, 2, valueToMatch)) {
                    return true;

                }
                break;

            case 3:

                if (rowNo == 1) {
                    if (checkForMark(2, 1, valueToMatch) && checkForMark(3, 0, valueToMatch)) {
                        return true;
                    }
                }

                if (rowNo == 3) {

                    if (checkForMark(2, 1, valueToMatch) && checkForMark(1, 0, valueToMatch)) {
                        return true;
                    }
                }
                break;

            default:
                break;
        }
        return false;
    }

}

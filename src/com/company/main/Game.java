package com.company.main;

import java.util.Scanner;

public class Game {

    // class variables
    private Boolean gameIsOver;
    private Boolean playerWon;
    private Integer gameTurns;
    private Scanner scanner;

    public Boolean getGameIsOver() {
        return gameIsOver;
    }

    // getters and setters
    public void setGameIsOver(Boolean gameIsOver) {
        this.gameIsOver = gameIsOver;
    }

    public Integer getGameTurns() {
        return gameTurns;
    }

    public void setGameTurns(Integer gameTurns) {
        this.gameTurns = gameTurns;
    }

    public Boolean getPlayerWon() {
        return playerWon;
    }

    public void setPlayerWon(Boolean playerWon) {
        this.playerWon = playerWon;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    // constructor
    public Game() {
        this.playerWon = false;
        this.gameTurns = 0;
        this.gameIsOver = false;
        this.scanner = new Scanner(System.in);
    }

    // class methods

    public String startAndRunGame(Grid mainBoard, AI aiPlayer) {

        // cycle game turns until a winner is found
        while (!this.gameIsOver) {
            gameTurns(aiPlayer, mainBoard);
        }

        // game is over
        System.out.println("Game is finished!");
        mainBoard.printTheBoard();

        if (mainBoard.getPlayerWon()) {
            return ("Player won the game. So much for Skynet..");
        } else {
            return ("AI won the game. The robot revolution is nigh.");
        }

    }

    // simulate a player's turn
    private void playerTurn(Grid grid) {

        grid.printTheBoard();

        // ask the user for their choice
        System.out.println("Enter a row number (1-3): ");
        int rowNo;

        // validation for row number
        do {
            while (!this.scanner.hasNextInt()) {
                System.out.println("Invalid input. Enter a number from 1 to 3: ");
                this.scanner.next();
            }
            rowNo = this.scanner.nextInt();
        } while (rowNo > 3 || rowNo < 0);

        System.out.println("Enter a space number (0-2): ");
        int spaceNo;

        // validation for space number
        do {
            while (!this.scanner.hasNextInt()) {
                System.out.println("Invalid input. Enter a number from 0 to 2: ");
                this.scanner.next();
            }
            spaceNo = this.scanner.nextInt();
        } while (spaceNo > 2 || spaceNo < 0);

        // player's mark can be changed here
        String playerMark = "X";

        // find out if the space is available
        if (grid.checkForOccupancy(rowNo, spaceNo)) {
            grid.placeMark(rowNo, spaceNo, playerMark);
        } else {
            System.out.println("That space is taken! Try a new combo.");
            playerTurn(grid);
        }
    }

    // cycle between ai/player turns
    private void gameTurns(AI aiPlayer, Grid mainBoard) {

        // ai's turn
        if (aiPlayer.getTheAITurn()) {
            aiPlayer.randomMark(mainBoard);
            aiPlayer.setTheAITurn(false);
        } else {
            // player's turn
            playerTurn(mainBoard);
            aiPlayer.setTheAITurn(true);
        }

        // if someone won then end the game
        if (mainBoard.checkTheBoard()) {
            this.gameIsOver = true;
        } else {
            gameTurns(aiPlayer, mainBoard);
        }
    }

}

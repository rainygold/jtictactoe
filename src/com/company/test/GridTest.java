package com.company.test;

import com.company.main.Grid;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

// All tests follow the triple A method (Arrange, Act, Assert)
class GridTest {

    @Test
    void testPopulateTheBoard() {

        // Arrange
        var boardToTest = new Grid();

        // Act
        boardToTest.populateTheBoard();

        // Assert
        assertFalse(boardToTest.getBoard().isEmpty());

        for (Object list : boardToTest.getBoard().values()) {
            for (String s : (ArrayList<String>) list) {
                assertEquals(s, "-");
            }
        }
    }

    // checkTheBoard tests also test the various private checkSpaces methods
    @Test
    void testCheckTheBoardWithNoWinner() {

        // Arrange
        var boardToTest = new Grid();

        // Act
        boardToTest.populateTheBoard();
        var result = boardToTest.checkTheBoard();

        // Assert
        assertFalse(result);
    }

    @Test
    void testCheckTheBoardWithPlayerWinner() {

        // Arrange
        var boardToTest = new Grid();
        var playerMark = "X";

        // Act
        boardToTest.populateTheBoard();
        boardToTest.placeMark(1, 0, playerMark);
        boardToTest.placeMark(2, 0, playerMark);
        boardToTest.placeMark(3, 0, playerMark);
        var result = boardToTest.checkTheBoard();

        // Assert
        assertTrue(result);
    }

    @Test
    void testCheckTheBoardWithAIWinner() {

        // Arrange
        var boardToTest = new Grid();
        var aiMark = "O";

        // Act
        boardToTest.populateTheBoard();
        boardToTest.placeMark(1, 0, aiMark);
        boardToTest.placeMark(2, 0, aiMark);
        boardToTest.placeMark(3, 0, aiMark);
        var result = boardToTest.checkTheBoard();

        // Assert
        assertTrue(result);
    }
}
package com.company.test;

import com.company.main.AI;
import com.company.main.Grid;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

// All tests follow the triple A method (Arrange, Act, Assert)
class AITest {

    @Test
    void testRandomMark() {

        // Arrange - create required objects
        var gridToTest = new Grid();
        var aiToTest = new AI();
        gridToTest.populateTheBoard();
        var resultToBeAsserted = "";

        // Act - search through grid to find the mark
        aiToTest.randomMark(gridToTest);

        for (Object list : gridToTest.getBoard().values()) {
            for (String s : (ArrayList<String>) list) {
                if (!s.equals("-")) {
                    resultToBeAsserted = s;
                }
            }
        }

        // Assert
        assertEquals(resultToBeAsserted, "O");


    }
}
package com.company.test;

import com.company.main.AI;
import com.company.main.Game;
import com.company.main.Grid;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.testng.Assert.assertTrue;

// all tests follow the triple A method (Arrange, Act, Assert)
class GameTest {

    @Test
    void testStartAndRunGameWithNoTurns() {

        // Arrange
        var gameToTest = new Game();
        gameToTest.setGameIsOver(true);
        var aiPlayer = new AI();
        var gameBoard = new Grid();
        gameBoard.populateTheBoard();

        // Act
        var result = gameToTest.startAndRunGame(gameBoard, aiPlayer);

        // Assert - method returns a string if the logic is correct
        assertTrue(result.contains("AI won the game."));
    }

    @Test
    void testStartAndRunGameWithPlayerWin() throws InterruptedException {

        // Arrange
        var aiPlayer = new AI();
        var gameBoard = new Grid();
        gameBoard.populateTheBoard();

        // Act
        var userInput = "1" + System.getProperty("line.separator") +
                "0" + System.getProperty("line.separator") +
                "2" + System.getProperty("line.separator") +
                "0" + System.getProperty("line.separator") +
                "3" + System.getProperty("line.separator") +
                "0";

        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        var gameToTest = new Game(); // for scanner to work properly
        var result = gameToTest.startAndRunGame(gameBoard, aiPlayer);

        // Assert - method returns a string if the logic is correct
        assertTrue(result.contains("Player won the game."));
    }
}